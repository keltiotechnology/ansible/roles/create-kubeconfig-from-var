# Create kubeconfig from var

Create a kubeconfig file from an ansible var.

## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| kubeconfig_content | Kubeconfig content | `string` | `` | yes |
| kubeconfig_path | Path to cluster kubeconfig | `string` | `{{ playbook_dir }}` | no |
| kubeconfig_filename | Path to cluster kubeconfig | `string` | `kubeconfig.yml` | no |
